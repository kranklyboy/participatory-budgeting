\documentclass{beamer}

\beamertemplatenavigationsymbolsempty

\usetheme{Boadilla}
\usecolortheme{dolphin}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{tikz}
\usepackage{dsfont}

\usetikzlibrary{arrows}

\begin{document}

\title[Participatory Budgeting]{Participatory Budgeting}
\subtitle{Algorithms and Complexity}
\author{Tobias Eidelpes}

\begin{frame}
	\maketitle
\end{frame}

\begin{frame}
	\frametitle{Table of Contents}
	\tableofcontents
\end{frame}

\section{Introduction}

\begin{frame}
	\frametitle{What is Participatory Budgeting?} \pause
	\begin{quote}
		Participatory Budgeting (PB) is a democratic process in which
		community members decide how to spend part of a public budget.
	\end{quote} \pause
    \vspace{1cm}
    \begin{itemize}
        \setlength{\itemsep}{1.1\baselineskip}
        \item Participatory part: community members propose projects \pause
        \item Budgeting part: each project requires a fixed amount of money
            \pause
        \item Goal: Fund the \emph{best} projects without exceeding the budget
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{A bit of background information} \pause
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item PB originated in Porto Alegre in 1990s \pause
        \item Rapid expansion and influx of immigrants increased inequality
            \pause
        \item PB as a tool to combat inequality \pause
        \item PB spread to Europe and North America \pause
        \item Today \$300M allocated
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{How does it work?} \pause
    \tikzstyle{blue} = [rectangle,rounded
    corners=3pt,draw=blue!50,fill=blue!20]
    \tikzstyle{red} = [rectangle,rounded corners=3pt,draw=red!50,fill=red!20]
    \tikzstyle{arrow} = [line width=1pt,->,>=triangle 60,draw=black!70]
    \begin{center}
        \begin{tikzpicture}[shorten >= 2pt,node distance=1.3cm]
            \node [blue] (design) {Design the process}; \pause
            \node [blue] (collect) [below of=design] {Collect ideas};
            \draw [arrow] (design) to (collect); \pause
            \node [blue] (develop) [below of=collect] {Develop feasible
                projects};
            \draw [arrow] (collect) to (develop); \pause
            \node [red] (vote) [below of=develop] {Vote on projects};
            \draw [arrow] (develop) to (vote); \pause
            \node [red] (aggregate) [below of=vote] {Aggregate votes \& fund
                winners};
            \draw [arrow] (vote) to (aggregate);
            \onslide<1->
        \end{tikzpicture}
    \end{center}
\end{frame}

\section{Modeling a PB scenario}

\begin{frame}
	\frametitle{A general framework for PB} \pause
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item Projects $P=\{p_1,\dots,p_m\}$ \pause
            \begin{itemize}
                \setlength{\itemsep}{.7\baselineskip}
                \item Each project $p\in P$ has associated cost
                    $c(p):P\rightarrow\mathbb{R}$ \pause
                \item Projects are either divisible or indivisible (discrete)
                    \pause
            \end{itemize}
        \item Select a set $A\subseteq P$ as \emph{winning projects} not
            exceeding total budget $B$ \pause
            \begin{itemize}
                \setlength{\itemsep}{.7\baselineskip}
                \item Discrete case: $\sum_{p\in A}c(p)\leq B$ \pause
                \item Divisible case: $\mu(p): P\rightarrow [0,1]$ with
                    $\sum_{p\in A}c(\mu(p))\leq B$
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{A general framework for PB ctd.}
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item Voters $V=\{v_1,\dots,v_n\}$ \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
                \item Express preferences over individual projects in $P$ or
                    over subsets in $\mathcal{P}(P) := \{P_v\,|\,P_v\subseteq
                    P\}$ \pause
                \item Preference elicitation is dependent on the input method
                    (approval-based, ranked orders) \pause
            \end{itemize}
        \item Aggregation methods \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
                \item Aggregation methods combine votes to determine a set
                    of winning projects \pause
                \item Are usually tied to the input method \pause
                \item Rules are used to select projects w.r.t. desired
                    properties of the outcome (fairness, welfare)
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Algorithms for PB}

\begin{frame}
    \frametitle{Input and aggregation methods}
    Example input methods: \pause
    \begin{itemize}
        \item Approval preferences \pause
        \item Ranked orders \pause
        \item Utility-based preferences \pause
    \end{itemize}
    \vspace{0.2cm}
    Example aggregation methods: \pause
    \begin{itemize}
        \item Maximizing social welfare \pause
        \item Maximizing voter satisfaction \pause
        \item Greedy selection \pause
        \item Fairness-based selection \pause
    \end{itemize}
    \vspace{0.2cm}
    Aggregation methods depend on how voters elicit their preferences.
\end{frame}

\begin{frame}
    \frametitle{Approval-based budgeting methods} \pause
    \begin{itemize}
        \item Suitable for discrete PB \pause
        \item Voters approve a subset of projects \pause
        \item Voter preferences are assumed to be \emph{dichotomous} \pause
        \item A \emph{satisfaction function} provides a metric for voter
            satisfaction \pause
    \end{itemize}
    \begin{block}{An approval-based budgeting scenario}
        A budgeting scenario is a tuple $E = (P,V,c,B)$ where $P =
        \{p_1,\dots,p_m\}$ is a set of projects, $V$ is a set of voters, $c :
        P\rightarrow\mathbb{N}$ is a cost function associating each project
        $p\in P$ with its cost $c(p)$ and $B\in\mathbb{N}$ is a budget limit. A
        voter $v\in V$ specifies $P_v\subseteq P$, containing all approved
        items.
    \end{block} \pause
    \begin{block}{Budgeting method $\mathcal{R}$}
        A budgeting method $\mathcal{R}$ takes a budgeting scenario $E$ and
        returns a bundle $A\subseteq P$ where the total cost of the items in
        $A$ does not exceed the budget limit $B$.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Satisfaction functions} \pause
    \begin{block}{Satisfaction function}
        A satisfaction function $sat : 2^P\times 2^P\rightarrow\mathbb{R}$ with
        a set $P$ of items, a voter $v$ and her approval set $P_v$ and a bundle
        $A\subseteq P$ provides the satisfaction $sat(P_v,A)$ of $v$ from the
        bundle $A$. The set of approved items by $v$ that end up in the winning
        bundle is denoted by $A_v = P_v\cap A$.
    \end{block} \pause
    \begin{exampleblock}{$sat_\#(P_v,A)$}
        $sat_\#(P_v,A) = |A_v|$: The satisfaction of voter $v$ is the number of
        funded items that are approved.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Satisfaction functions ctd.}
    \begin{exampleblock}{$sat_\$(P_v,A)$}
        $sat_{\$}(P_v,A) = \sum_{p\in A_v}{c(p) = c(A_v)}$: The satisfaction of voter
        $v$ is the total cost of her approved and funded items.
    \end{exampleblock} \pause
    \begin{exampleblock}{$sat_{0/1}(P_v,A)$}
        \[ sat_{0/1}(P_v,A) =
            \begin{cases}
                1 & \text{if } |A_v|>0 \\
                0 & \text{otherwise}
            \end{cases}
        \]
        A voter $v$ has satisfaction 1 if at least one of her approved items is
        funded and 0 otherwise.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Rules for selecting a winning bundle} \pause
    {\Large Let $sat$ be a satisfaction function:} \pause
    \begin{block}{Max rules}
        The rule $\mathcal{R}_{sat}^m$ selects a bundle which maximizes the sum
        of voters' satisfaction: $\mathsf{max}_{A\subseteq P}\sum_{v\in
        V}{sat(P_v,A)}$
    \end{block} \pause
    \begin{block}{Greedy rules}
        The rule $\mathcal{R}_{sat}^g$ iteratively adds an item $p\in P$ to $A$,
        seeking to maximize $\sum_{v\in V}{sat(P_v,A\cup\{p\})}$.
    \end{block} \pause
    \begin{block}{Proportional greedy rules}
        The rule $\mathcal{R}_{sat}^p$ iteratively adds an item $p\in P$ to $A$
        seeking to maximize the sum of satisfaction per unit of cost.
    \end{block}
\end{frame}

\begin{frame}
    \frametitle{Example budgeting scenarios} \pause
    \begin{block}{A budgeting scenario}
        Items $P = \{p_2,p_3,p_4,p_5,p_6\}$ and their associated cost $p_i$
        where $p_i$ costs $i$. Budget limit $B=10$ and 5 voters vote
        $v_1=\{p_2,p_5,p_6\}$, $v_2=\{p_2,p_3,p_4,p_5\}$, $v_3=\{p_3,p_4,p_5\}$,
        $v_4=\{p_4,p_5\}$ and $v_5=\{p_6\}$.
    \end{block} \pause
    \begin{exampleblock}{Combining max rule with $sat_\#$}
        Under $\mathcal{R}_{|A_v|}^m$ the winning bundle is $\{p_2,p_3,p_5\}$.
        The total satisfaction is 8.
    \end{exampleblock} \pause
    \begin{exampleblock}{Combining greedy rule with $sat_\#$}
        Under $\mathcal{R}_{|A_v|}^g$ the winning bundle is $\{p_4,p_5\}$ (first
        selecting $p_5$). The total satisfaction is 7.
    \end{exampleblock} \pause
    \begin{exampleblock}{Combining max rule with $sat_{0/1}$}
        Under $\mathcal{R}^m_{sat_{0/1}}$ the winning bundle is $\{p_4,p_6\}$,
        achieving max satisfaction.
    \end{exampleblock}
\end{frame}

\section{Complexity of PB algorithms}

\begin{frame}
    \frametitle{Complexity of algorithms} \pause
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item Computing winners using greedy rules ($\mathcal{R}^g_{sat}$) can
            be done in polynomial time: \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
                \item these rules are defined through efficient iterative
                    processes \pause
                \item however: making a series of locally optimal choices does
                    not always lead to a globally optimal choice \pause
            \end{itemize}
        \item Max rules ($\mathcal{R}^m_{sat}$) are generally NP-hard \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
            \item $\mathcal{R}^m_{|A_v|}$ can be solved in polynomial time
                \pause
            \item $\mathcal{R}^m_{sat_{0/1}}$: finding a bundle with at least a
                given total satisfaction is NP-hard \pause
            \item satisfaction functions can be modeled as integer linear
                programs
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Complexity of algorithms ctd.}
    {\large Dealing with \emph{intractability}:} \pause
    \vspace{.3cm}
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item Provide an approximation algorithm, sacrificing exactness \pause
            \begin{itemize}
                \setlength{\itemsep}{0.4\baselineskip}
                \item No algorithm with approx. ratio better than $1-1/\epsilon$
                    exists for $\mathcal{R}^m_{sat_{0/1}}$ \pause
            \end{itemize}
        \item Fixed-parameter tractability: fix one parameter to solve problem
            in reasonable amount of time \pause
            \begin{itemize}
                \setlength{\itemsep}{0.4\baselineskip}
            \item Fix parameter $m$ (the number of items) \pause
            \item Fix parameter $n$ (the number of voters)
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Axioms for PB algorithms}

\begin{frame}
    \frametitle{Comparing algorithms} \pause
    \begin{itemize}
        \setlength{\itemsep}{.9\baselineskip}
        \item Compare algorithms by using axioms \pause
        \item Axioms provide \emph{guidelines} for choosing an algorithm \pause
        \item Some might be desirable, others are not a drawback, if not
            satisfied \pause
        \item Desirable properties: \emph{fairness}, \emph{strategyproofness}
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Comparing algorithms—Discount Monotonicity} \pause
    \begin{block}{Discount Monotonicity}
        Suppose a budgeting algorithm $\mathcal{R}$ outputs a winning set of
        projects $A$. The cost of project $p\in A$ is lowered (discounted)
        compared to the previous cost. $\mathcal{R}$ should output another
        winning set $A'$ where project $p$ is not implemented to a lesser
        degree.
    \end{block} \pause
    \begin{block}{A budgeting scenario}
        Items $P = \{p_2,p_3,p_4,p_5,p_6\}$ and their associated cost $p_i$
        where $p_i$ costs $i$. Budget limit $B=10$ and 5 voters vote
        $v_1=\{p_2,p_5,p_6\}$, $v_2=\{p_2,p_3,p_4,p_5\}$, $v_3=\{p_3,p_4,p_5\}$,
        $v_4=\{p_4,p_5\}$ and $v_5=\{p_6\}$.
    \end{block} \pause
    \begin{exampleblock}{Discount Monotonicity Example}
        Under $\mathcal{R}^m_{|A_v|}$ the winning bundle is $\{p_2,p_3,p_5\}$.
        After discounting $p_2$ to $p_1$, we still get $\{p_1,p_3,p_5\}$.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Comparing algorithms—Limit Monotonicity} \pause
    \begin{block}{Limit Monotonicity}
        A budgeting method $\mathcal{R}$ satisfies Limit Monotonicity if for a
        pair of budgeting scenarios $E=(P,V,c,B)$, $E'=(P,V,c,B+1)$ and with no
        project that costs exactly $B+1$, for each project $p\in P$
        $p\in\mathcal{R}(E)\implies p\in\mathcal{R}(E')$ holds.
    \end{block} \pause
    \begin{block}{A budgeting scenario}
        Items $P=\{a_1,b_1,c_1\}$ (all with unit cost), budget limit $B=1$ and 4
        voters vote $v_1=\{a_1\}$, $v_2=\{a_1,b_1\}$, $v_3=\{b_1,c_1\}$ and
        $v_4=\{c_1\}$.
    \end{block} \pause
    \begin{exampleblock}{Limit Monotonicity Example}
        Under $\mathcal{R}^m_{sat_{0/1}}$ a winning bundle might be $\{b_1\}$.
        Increasing the budget limit to 2 results in $\{a_1,c_1\}$.
    \end{exampleblock}
\end{frame}

\begin{frame}
    \frametitle{Applying the two axioms to the example algorithms} \pause
    \begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
        \item Discount Monotonicity: \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
                \item All 3 algorithms ($\mathcal{R}^m_{|A_v|}$,
                    $\mathcal{R}^g_{|A_v|}$, $\mathcal{R}^m_{sat_{0/1}}$)
                    satisfy discount monotonicity \pause
                \item Decreasing the cost increases the attractiveness of an
                    item \pause
                \item Not true for algorithms that measure satisfaction by
                    maximizing the cost of winning projects \pause
            \end{itemize}
        \item Limit Monotonicity: \pause
            \begin{itemize}
                \setlength{\itemsep}{.4\baselineskip}
                \item No discussed algorithm satisfies limit monotonicity \pause
                \item Greedy heuristics fail when a project fits into the new
                    budget, providing higher satisfaction \pause
                \item Increasing the budget might lead to projects being dropped
            \end{itemize}
    \end{itemize}
\end{frame}

\section{Conclusion}

\begin{frame}
	\frametitle{Conclusion} \pause
	\begin{itemize}
        \setlength{\itemsep}{1\baselineskip}
		\item PB: What is it? \pause
		\item Computational aspects \pause
		\item Aggregation algorithms \pause
		\item Complexity of algorithms \pause
		\item Comparison of algorithms using axioms
	\end{itemize}
\end{frame}

\begin{frame}
	\centering
	\Large
	Thank you for your attention! \\
    \begin{figure}
        \centering
        \includegraphics[width=.5\textwidth]{voting_referendum.png}
        \captionsetup{labelformat=empty}
        \caption{\tiny [\url{https://xkcd.com/2225}]}
    \end{figure}
\end{frame}

\end{document}
